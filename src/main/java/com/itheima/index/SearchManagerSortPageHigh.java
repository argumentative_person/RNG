package com.itheima.index;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.junit.Test;
import org.wltea.analyzer.lucene.IKAnalyzer;

import javax.print.DocFlavor;
import java.io.File;

// 索引处理类
public class SearchManagerSortPageHigh {



    // 索引库位置
    private final static String INDEX_PATH = "C:\\czbk\\备课\\01-高级开发工程师--九次课\\06-高级开发工程师系列-Luence全文检索基础-over\\索引库\\index07";


    /*

    /**
     * 搜索索引
     */
    @Test
    public  void searchIndex() throws  Exception{
        //搜索条件的创建
        //1. 创建分析器对象（Analyzer），用于分词
        //Analyzer analyzer = new StandardAnalyzer();
        Analyzer analyzer = new IKAnalyzer();//es有整合ik

        // 语义搜索
        // 为什么要提供一个analyzer对象，作用是什么？
        QueryParser queryParser = new MultiFieldQueryParser(new String[]{"bookName"},analyzer);
        //2. 创建查询对象（Query）
        Query query = queryParser.parse("我正在传智播客学习java");
        // 执行搜索
        this.searchMain(query,1,10);
    }



    // 词条搜索 jvm
    @Test
    public void termQuery(){
        try{
            // 建立一个词条对象
            //Term term = new Term("bookName","我正在传智播客学习java");
            Term term = new Term("bookId","1");//StringField
            // 词条搜索
            Query query = new TermQuery(term);
            // 执行搜索
            this.searchMain(query,1,10);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }


    // 范围搜索
    @Test
    public void rangeQuery(){
        try{
            // 构建一个搜索范围查询NumericRangeQuery<?> 是有你搜索域来决定 bookPrice价格是Float,
            // system.out.println(80.8f);//double
            // 参数一：域名  参数二/参数三：具体范围值  ，参数四和参数五:boolean类型，代表释放包含，如果为false不包含，true包含。
            NumericRangeQuery<Float> numericRangeQuery = NumericRangeQuery.newFloatRange("bookPrice", 80f, 100f, true, true);
            // 执行搜索
            System.out.println("================1==============");
            this.searchMain(numericRangeQuery,1,2);
            System.out.println("================2==============");
            this.searchMain(numericRangeQuery,2,2);
            System.out.println("================3==============");
            this.searchMain(numericRangeQuery,3,2);


            System.out.println("=============all================");
            this.searchMain(numericRangeQuery,1,6);


        }catch(Exception ex){
            ex.printStackTrace();
        }
    }



    @Test
    public void testBooleanQuery() throws  Exception{
        this.booleanQuery("传智播客java",null,80f,100f);
    }




    public void booleanQuery(String keyword,String brandId,Float startPirce,Float endPrice) throws  Exception{

        // 复合搜索
        BooleanQuery booleanQuery = new BooleanQuery();
        //1------------------------------------语义搜索
        //Analyzer analyzer = new StandardAnalyzer();
        if(keyword!=null) {
            Analyzer analyzer = new IKAnalyzer();//es有整合ik
            // 语义搜索
            // 为什么要提供一个analyzer对象，作用是什么？
            QueryParser queryParser = new MultiFieldQueryParser(new String[]{"bookName"}, analyzer);
            //2. 创建查询对象（Query）
            Query queryparse = queryParser.parse(keyword);
            // 确定关系 MUST ---AND   SHOULD --- or关系 MUST_NOT :不等于
            booleanQuery.add(queryparse, BooleanClause.Occur.MUST);
        }

        if(brandId!=null) {
            // 2------------------------------------词条搜索
            //Term term = new Term("bookName","我正在传智播客学习java");
            Term term = new Term("bookId", brandId);//StringField
            // 词条搜索
            Query termQuery = new TermQuery(term);
            booleanQuery.add(termQuery, BooleanClause.Occur.MUST);
        }


        if(startPirce!=null && endPrice!=null) {
            // 3-----------------------------------数字的范围搜索
            // 参数一：域名  参数二/参数三：具体范围值  ，参数四和参数五:boolean类型，代表释放包含，如果为false不包含，true包含。
            NumericRangeQuery<Float> numericRangeQuery = NumericRangeQuery.newFloatRange("bookPrice", startPirce, endPrice, true, true);
            booleanQuery.add(numericRangeQuery, BooleanClause.Occur.MUST);
        }

        //执行搜索
        this.searchMain(booleanQuery,1,10);
    }


    public void searchMain(Query query,int pageNo,int pageSize) throws  Exception{
        //3. 创建索引数据读取对象（IndexReader），把索引数据读取到内存中
        //4. 创建索引库目录对象（Directory），指定索引库的位置
        Directory directory = FSDirectory.open(new File(INDEX_PATH));
        IndexReader indexReader = DirectoryReader.open(directory);

        //5. 创建索引搜索对象（IndexSearcher）
        IndexSearcher indexSearcher = new IndexSearcher(indexReader);

        // 高亮处理
        // 建立高亮的分词其对象
        Analyzer analyzer = new IKAnalyzer();
        SimpleHTMLFormatter simpleHTMLFormatter = new SimpleHTMLFormatter("<font class='skcolor_ljg'>","</span>");
        Highlighter highlighter = new Highlighter(simpleHTMLFormatter,new QueryScorer(query));

        // 排序 参数1：排序域名 参数二：类型 参数三：true降序/false升序
        SortField sortField = new SortField("bookPrice", SortField.Type.FLOAT,false);
        Sort sort = new Sort(sortField);


        // 处理搜索结果,执行搜索 ,返回topics对象,10代表最多返回10条，
        //TopDocs topDocs = indexSearcher.search(query, 10);
        TopDocs topDocs = indexSearcher.search(query, 10,sort);

        // 命中结果总记录数
        int totalHits = topDocs.totalHits;
        System.out.println(" 一共命中到： " + totalHits+ "条");
        // pagehelper
        int start = (pageNo - 1) * pageSize;//4
        int end = pageSize + start;//6
        if(end>=totalHits){
            end = totalHits;
        }

        //es--lucene---安全备份
        //索引库体积过大页是很慢---拆分---集群分布式 1G --- 200mb --安全备份---ES天然支持群


        // 第1页： start = 0 end = 2  [1,2]
        //        // 第2页： start = 2 end = 4  [3,4]
        //        // 第3页： start = 4 end = 6  [5,6]
        //        // 第4页： start = 6 end = 8  [7,8]
        //文档对象Document存储的数组容器
        ScoreDoc[] scoreDocs = topDocs.scoreDocs;

        for(int i=start;i<end;i++){
            ScoreDoc scoreDoc = scoreDocs[i];
            // 获取文档对象 `scoreDoc.doc ` 就是文档id
            Document document = indexReader.document(scoreDoc.doc);//document ---POJO
            System.out.println("========获取高亮内容===========");
            String bookName = getHightMessage("bookName",analyzer,document,highlighter);
            //String bookDesc = getHightMessage("bookDesc",analyzer,document,highlighter);
            if(bookName!=null){
                System.out.println("你高亮的名字是：" + bookName);
            }else{
                System.out.println(document.get("bookName"));
            }
            System.out.println(document.get("bookId"));
            System.out.println(document.get("bookPic"));
            System.out.println(document.get("bookPrice"));
            System.out.println(document.get("bookDesc"));

        }
        //6. 释放资源 es
        indexReader.close();
    }


    //
    private String getHightMessage(String fieldName,Analyzer analyzer,Document document,Highlighter highlighter) throws  Exception{
        TokenStream tokenStream = analyzer.tokenStream(fieldName, document.get(fieldName));
        String bookName = highlighter.getBestFragment(tokenStream, document.get(fieldName));
        return bookName;
    }

}
