package com.itheima.index;

import com.itheima.dao.BaseDaoImpl;
import com.itheima.pojo.Book;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.cjk.CJKAnalyzer;
import org.apache.lucene.analysis.cn.smart.SmartChineseAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.junit.Test;
import org.wltea.analyzer.lucene.IKAnalyzer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

// 索引处理类
public class IndexManager {



    // 索引库位置
    private final static String INDEX_PATH = "C:\\czbk\\备课\\01-高级开发工程师--九次课\\06-高级开发工程师系列-Luence全文检索基础-over\\索引库\\index07";


    /**
     * 创建索引
     */
    @Test
    public void createIndex() throws  Exception{
        // 原始数据
        List<Book> books = BaseDaoImpl.findBooks();
        //1. 采集数据准备文档（Document） 看见POJO
        List<Document> documents = new ArrayList<Document>();
        for (Book book : books) {
            Document document  = new Document();
            // TextField 是什么？它能够影响什么决定什么？
            //TextField
            //StringField
            //DoubleField/FloatField/IntField/LongField
            //SortedField

            //参数说明：
            // 参数1：域名，名字，比如bookId,bookName
            // 参数2：域值，
            // 参数3：Field.Store.YES 代表你是否存储索引库这个值你来决定，如果NO,代表在返回是null.
            // 1 2 3  100 / 姓名，年龄，电话号码，邮箱，地址
            // 是否分词：决定你要你要把你数据切分成成一个个的词。 Y
            // 是否索引：决定你该Field是不是要参与搜索也不会建立document的关系。Y
            document.add(new StringField("bookId",String.valueOf(book.getId()), Field.Store.YES));
            document.add(new TextField("bookName",book.getBookname(), Field.Store.YES));
            document.add(new StoredField("bookPic",book.getPic()));
            //rangquery---前提条件：你域：必须是数字域 8<=bookPrice<=100
            document.add(new FloatField("bookPrice",book.getPrice(), Field.Store.YES));
            document.add(new StringField("bookPrice2",book.getPrice()+"", Field.Store.YES));
            document.add(new TextField("bookDesc",book.getBookdesc(), Field.Store.YES));
            documents.add(document);

        }


        //2. 创建索引库配置对象（IndexWriterConfig）配置索引库
        //2-1：创建分析器对象（Analyzer），用于分词
        //Analyzer analyzer = new StandardAnalyzer();// 标准分词器---英文分词器---my name is  xuxxxx ,我的是名字：xxx
        // CJKAnalyzer分词器 二元切分  java从入门到精通 ,java|从入|入门|门到|到精|精通
        //Analyzer analyzer = new CJKAnalyzer();
        // smartcn 分词器
        //Analyzer analyzer = new SmartChineseAnalyzer();
        // 改成中文分词器
        Analyzer analyzer = new IKAnalyzer();

        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(Version.LUCENE_4_10_3,analyzer);

        //3. 创建索引库的目录对象（Directory），指定索引库的存储位置
        Directory directory = FSDirectory.open(new File(INDEX_PATH));


        //4. 创建索引库操作对象（IndexWriter），操作索引库
        IndexWriter writer = new IndexWriter(directory,indexWriterConfig);
        //使用IndexWriter对象，把文档对象写入索引库
        for (Document document : documents) {
            //将文档对象写入到指定的索引库
            writer.addDocument(document);
        }
        //释放资源
        writer.close();

        System.out.println("==================索引创建成功======================");
    }


    /**
     * 搜索索引
     */
    @Test
    public  void searchIndex() throws  Exception{
        //搜索条件的创建
        //1. 创建分析器对象（Analyzer），用于分词
        //Analyzer analyzer = new StandardAnalyzer();
        Analyzer analyzer = new IKAnalyzer();//es有整合ik
        // 为什么要提供一个analyzer对象，作用是什么？
        QueryParser queryParser = new MultiFieldQueryParser(new String[]{"bookName"},analyzer);
        //2. 创建查询对象（Query）
        Query query = queryParser.parse("毛泽东");

        //3. 创建索引数据读取对象（IndexReader），把索引数据读取到内存中
        //4. 创建索引库目录对象（Directory），指定索引库的位置
        Directory directory = FSDirectory.open(new File(INDEX_PATH));
        IndexReader indexReader = DirectoryReader.open(directory);

        //5. 创建索引搜索对象（IndexSearcher）
        IndexSearcher indexSearcher = new IndexSearcher(indexReader);
        // 处理搜索结果,执行搜索 ,返回topics对象,10代表最多返回10条，
        TopDocs topDocs = indexSearcher.search(query, 10);
        // 命中结果总记录数
        int totalHits = topDocs.totalHits;
        System.out.println(" 一共命中到： " + totalHits+ "条");
        //文档对象Document存储的数组容器
        ScoreDoc[] scoreDocs = topDocs.scoreDocs;
        for (ScoreDoc scoreDoc : scoreDocs) {
            // 获取文档对象 `scoreDoc.doc ` 就是文档id
            Document document = indexReader.document(scoreDoc.doc);
            System.out.println(document.get("bookId"));
            System.out.println(document.get("bookName"));
            System.out.println(document.get("bookPic"));
            System.out.println(document.get("bookPrice"));
            System.out.println(document.get("bookDesc"));
            System.out.println("===================");
        }

        //6. 释放资源 es
        indexReader.close();
    }



    // 删除索引 只会删除docuemnt 不会删除词条
    @Test
    public void deleteIndex() throws  Exception{
        //1-1.创建分析器对象（Analyzer），用于分词
        Analyzer analyzer = new IKAnalyzer();
        //1.创建索引库配置对象（IndexWriterConfig），配置索引库
        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(Version.LUCENE_4_10_3,analyzer);

        //2.创建索引库的目录对象（Directory），指定索引库的位置
        //3. 创建索引库的目录对象（Directory），指定索引库的存储位置
        Directory directory = FSDirectory.open(new File(INDEX_PATH));

        //3.创建索引库的操作对象（IndexWriter），操作索引库
        IndexWriter writer = new IndexWriter(directory,indexWriterConfig);
        //3-1.创建条件对象（Term） delete from index05 where bookName ="java"
        Term term = new Term("bookName","java");
        //3-2.使用IndexWriter对象，执行删除
        writer.deleteDocuments(term);
        //3-3.释放资源
        writer.close();
    }


    // 删除全部
    @Test
    public void deleteIndexAll() throws  Exception{
        //1-1.创建分析器对象（Analyzer），用于分词
        Analyzer analyzer = new IKAnalyzer();
        //1.创建索引库配置对象（IndexWriterConfig），配置索引库
        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(Version.LUCENE_4_10_3,analyzer);

        //2.创建索引库的目录对象（Directory），指定索引库的位置
        //3. 创建索引库的目录对象（Directory），指定索引库的存储位置
        Directory directory = FSDirectory.open(new File(INDEX_PATH));

        //3.创建索引库的操作对象（IndexWriter），操作索引库
        IndexWriter writer = new IndexWriter(directory,indexWriterConfig);
        // 删除全部，删库跑路
        writer.deleteAll();

        // 释放资源
        writer.close();
    }



    //修改索引
    @Test
    public void updateIndex() throws  Exception{
        //1-1.创建分析器对象（Analyzer），用于分词
        Analyzer analyzer = new IKAnalyzer();
        //1.创建索引库配置对象（IndexWriterConfig），配置索引库
        IndexWriterConfig indexWriterConfig = new IndexWriterConfig(Version.LUCENE_4_10_3,analyzer);
        //2.创建索引库的目录对象（Directory），指定索引库的位置
        //3. 创建索引库的目录对象（Directory），指定索引库的存储位置
        Directory directory = FSDirectory.open(new File(INDEX_PATH));
        //3.创建索引库的操作对象（IndexWriter），操作索引库
        IndexWriter writer = new IndexWriter(directory,indexWriterConfig);

        //update index05 set bookId=1,bookName='mybatis spring spring' where bookId=1

        // 修改核心--应该要查询出来然后
        Document document = new Document();
        document.add(new StringField("bookId","1", Field.Store.YES));
        document.add(new StringField("bookName","mybatis spring spring", Field.Store.YES));

        //修改的条件
        Term term = new Term("bookId","1");

        //修改执行
        writer.updateDocument(term,document);
        //释放资源
        writer.close();

    }




























}
