package com.itheima.index;

import com.itheima.dao.BaseDaoImpl;
import com.itheima.pojo.Book;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.junit.Test;
import org.wltea.analyzer.lucene.IKAnalyzer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

// 索引处理类
public class SearchManager {



    // 索引库位置
    private final static String INDEX_PATH = "C:\\czbk\\备课\\01-高级开发工程师--九次课\\06-高级开发工程师系列-Luence全文检索基础-over\\索引库\\index07";


    /*

    /**
     * 搜索索引
     */
    @Test
    public  void searchIndex() throws  Exception{
        //搜索条件的创建
        //1. 创建分析器对象（Analyzer），用于分词
        //Analyzer analyzer = new StandardAnalyzer();
        Analyzer analyzer = new IKAnalyzer();//es有整合ik

        // 语义搜索
        // 为什么要提供一个analyzer对象，作用是什么？
        QueryParser queryParser = new MultiFieldQueryParser(new String[]{"bookName"},analyzer);
        //2. 创建查询对象（Query）
        Query query = queryParser.parse("我正在传智播客学习java");
        // 执行搜索
        this.searchMain(query);
    }



    // 词条搜索 jvm
    @Test
    public void termQuery(){
        try{
            // 建立一个词条对象
            //Term term = new Term("bookName","我正在传智播客学习java");
            Term term = new Term("bookId","1");//StringField
            // 词条搜索
            Query query = new TermQuery(term);
            // 执行搜索
            this.searchMain(query);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }


    // 范围搜索
    @Test
    public void rangeQuery(){
        try{
            // 构建一个搜索范围查询NumericRangeQuery<?> 是有你搜索域来决定 bookPrice价格是Float,
            // system.out.println(80.8f);//double
            // 参数一：域名  参数二/参数三：具体范围值  ，参数四和参数五:boolean类型，代表释放包含，如果为false不包含，true包含。
            NumericRangeQuery<Float> numericRangeQuery = NumericRangeQuery.newFloatRange("bookPrice", 80f, 100f, true, true);
            // 执行搜索
            this.searchMain(numericRangeQuery);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }



    @Test
    public void testBooleanQuery() throws  Exception{
        this.booleanQuery("传智播客java",null,80f,100f);
    }




    public void booleanQuery(String keyword,String brandId,Float startPirce,Float endPrice) throws  Exception{

        // 复合搜索
        BooleanQuery booleanQuery = new BooleanQuery();
        //1------------------------------------语义搜索
        //Analyzer analyzer = new StandardAnalyzer();
        if(keyword!=null) {
            Analyzer analyzer = new IKAnalyzer();//es有整合ik
            // 语义搜索
            // 为什么要提供一个analyzer对象，作用是什么？
            QueryParser queryParser = new MultiFieldQueryParser(new String[]{"bookName"}, analyzer);
            //2. 创建查询对象（Query）
            Query queryparse = queryParser.parse(keyword);
            // 确定关系 MUST ---AND   SHOULD --- or关系 MUST_NOT :不等于
            booleanQuery.add(queryparse, BooleanClause.Occur.MUST);
        }

        if(brandId!=null) {
            // 2------------------------------------词条搜索
            //Term term = new Term("bookName","我正在传智播客学习java");
            Term term = new Term("bookId", brandId);//StringField
            // 词条搜索
            Query termQuery = new TermQuery(term);
            booleanQuery.add(termQuery, BooleanClause.Occur.MUST);
        }

        if(startPirce!=null && endPrice!=null) {
            // 3-----------------------------------数字的范围搜索
            // 参数一：域名  参数二/参数三：具体范围值  ，参数四和参数五:boolean类型，代表释放包含，如果为false不包含，true包含。
            NumericRangeQuery<Float> numericRangeQuery = NumericRangeQuery.newFloatRange("bookPrice", startPirce, endPrice, true, true);
            booleanQuery.add(numericRangeQuery, BooleanClause.Occur.MUST);
        }

        //执行搜索
        this.searchMain(booleanQuery);
    }


    public void searchMain(Query query) throws  Exception{
        //3. 创建索引数据读取对象（IndexReader），把索引数据读取到内存中
        //4. 创建索引库目录对象（Directory），指定索引库的位置
        Directory directory = FSDirectory.open(new File(INDEX_PATH));
        IndexReader indexReader = DirectoryReader.open(directory);

        //5. 创建索引搜索对象（IndexSearcher）
        IndexSearcher indexSearcher = new IndexSearcher(indexReader);
        // 处理搜索结果,执行搜索 ,返回topics对象,10代表最多返回10条，
        TopDocs topDocs = indexSearcher.search(query, 10);

        // 命中结果总记录数
        int totalHits = topDocs.totalHits;
        System.out.println(" 一共命中到： " + totalHits+ "条");
        //文档对象Document存储的数组容器
        ScoreDoc[] scoreDocs = topDocs.scoreDocs;
        for (ScoreDoc scoreDoc : scoreDocs) {
            // 获取文档对象 `scoreDoc.doc ` 就是文档id
            Document document = indexReader.document(scoreDoc.doc);//document ---POJO
            System.out.println(document.get("bookId"));
            System.out.println(document.get("bookName"));
            System.out.println(document.get("bookPic"));
            System.out.println(document.get("bookPrice"));
            System.out.println(document.get("bookDesc"));
            System.out.println("===================");
        }

        //6. 释放资源 es
        indexReader.close();
    }









}
