package com.itheima.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author  xuchengfeifei
 * @description: 书籍表
 */
@Data // set get
@NoArgsConstructor // 无参构造数据
@AllArgsConstructor // 所有构造函数
@ToString// tostring
public class Book {
    // 书籍编号
    private Integer id;
    // 书籍名
    private String bookname;
    // 书籍价格
    private Float price;
    // 书籍的封面
    private String pic;
    // 书籍的描述
    private String bookdesc;
}
