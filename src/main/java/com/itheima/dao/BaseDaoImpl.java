package com.itheima.dao;

import com.itheima.pojo.Book;
import sun.plugin2.applet.context.NoopExecutionContext;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 *  书籍管理
 */
public class BaseDaoImpl {

    private final static String URL = "jdbc:mysql://localhost:3306/book";
    private final static String USERNAME = "root";
    private final static String PASSWORD = "root";

    public static List<Book> findBooks(){
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        try {
            // 1: 建立链接对象
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(URL,USERNAME,PASSWORD);
            // 2: 准备 sql
            String sql = "SELECT\n" +
                    "\tid,\n" +
                    "\tbookname,\n" +
                    "\tprice,\n" +
                    "\tpic,\n" +
                    "\tbookdesc\n" +
                    "FROM\n" +
                    "\tbook ";
            // 3：建立预处理对象PrepareStatmenet
            preparedStatement = connection.prepareStatement(sql);//err
            // 4: 返回处理的ResultSet
            rs = preparedStatement.executeQuery();
            // 5: 遍历resultset
            List<Book> bookList = new ArrayList<Book>();
            while(rs.next()){
                Book book = new Book();
                book.setId(rs.getInt("id"));
                book.setPrice(rs.getFloat("price"));
                book.setBookdesc(rs.getString("bookdesc"));
                book.setPic(rs.getString("pic"));
                book.setBookname(rs.getString("bookname"));
                //追加数据到集合中。
                bookList.add(book);
            }
            return bookList;
        }catch (Exception ex){
            ex.printStackTrace();
            return null;
        }finally {
            // 6: 释放资源 rs statment connection
            try {
                if(rs!=null)rs.close();
                if(preparedStatement!=null)preparedStatement.close();
                if(connection!=null)connection.close();
            }catch (SQLException ex){
                ex.printStackTrace();
            }
        }
    }

    public static void main(String[] args){
        List<Book> books = findBooks();
        for (Book book : books) {
            System.out.println(book);
        }

    }


}
